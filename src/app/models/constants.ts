export enum FrequencyUnit {
    Hz = 0,
    kHz,
    MHz,
    mHz,
    uHz,
}

export enum Channel {
    One = 0,
    Two
}
export enum WaveForm {
    W01SINE = "Sine",
    W02SQUARE = "Square",
    W03PULSE = "Pulse",
    W04TRIANGLE = "Triangle",
    W05PARTIAL_SINE = "Partial Sine",
    W06CMOS = "CMOS",
    W07DC = "DC",
    W08HALF_WAVE = "Half Wave",
    W09FULL_WAVE = "Full Wave",
    W10POS_LADDER = "Positive Ladder",
    W11NEG_LADDER = "Negative Ladder",
    W12NOISE = "Noise",
    W13EXP_RISE = "Exp Rise",
    W14EXP_DECAY = "Exp Decay",
    W15MULTI_TONE = "Multi Tone",
    W16SINC = "Sinc",
    W17LORENZ = "Lorenz",
    W18ARBITRARY = "Arbitrary"
}

export enum WaveFormCodes {
    W01SINE = 0,
    W02SQUARE = 1,
    W03PULSE = 2,
    W04TRIANGLE = 3,
    W05PARTIAL_SINE = 4,
    W06CMOS = 5,
    W07DC = 6,
    W08HALF_WAVE = 7,
    W09FULL_WAVE = 8,
    W10POS_LADDER = 9,
    W11NEG_LADDER = 10,
    W12NOISE = 11,
    W13EXP_RISE = 12,
    W14EXP_DECAY = 13,
    W15MULTI_TONE = 14,
    W16SINC = 15,
    W17LORENZ = 16,
    W18ARBITRARY = 101
}

export abstract class SerialConstants {
    static readonly MODEL = ':r00=';
    static readonly SERIAL_NUMBER = ':r01=';

    static readonly C1_WAVE = ':w21=';
    static readonly C2_WAVE = ':w22=';
    static readonly C1_FREQUENCY = ':w23=';
    static readonly C2_FREQUENCY = ':w24=';
    static readonly C1_AMPLITUDE = ':w25=';
    static readonly C2_AMPLITUDE = ':w26=';
    static readonly C1_OFFSET = ':w27=';
    static readonly C2_OFFSET = ':w28=';
    static readonly C1_DUTY = ':w29=';
    static readonly C2_DUTY = ':w30=';

}

export abstract class SerialPhraseConstants {
    static readonly MODEL = 'model';
    static readonly SERIAL_NUMBER = 'serial-number';

    static readonly C1_WAVE = 'c1-wave';
    static readonly C2_WAVE = 'c2-wave';
    static readonly C1_FREQUENCY = 'c1-freq';
    static readonly C2_FREQUENCY = 'c2-freq';
    static readonly C1_AMPLITUDE = 'c1-amp';
    static readonly C2_AMPLITUDE = 'c2-amp';
    static readonly C1_OFFSET = 'c1-offset';
    static readonly C2_OFFSET = 'c2-offset';
    static readonly C1_DUTY = 'c1-duty';
    static readonly C2_DUTY = 'c2-duty';
    
    static readonly CONNECTED = 'connected'
    static readonly UNKNOWN = 'unknown';
}

