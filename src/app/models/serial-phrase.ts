export interface SerialPhrase {
    key: string;
    message?: string;
}

