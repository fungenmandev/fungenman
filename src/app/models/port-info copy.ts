export interface PortInfo {
    path: string;
    manufacturer?: string;
}
