import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputPanelAmplitudeOffsetDutyComponent } from './output-panel-amplitude-offset-duty.component';

describe('OutputPanelAmplitudeComponent', () => {
  let component: OutputPanelAmplitudeOffsetDutyComponent;
  let fixture: ComponentFixture<OutputPanelAmplitudeOffsetDutyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputPanelAmplitudeOffsetDutyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputPanelAmplitudeOffsetDutyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
