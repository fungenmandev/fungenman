import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

enum UnitType {
  AMPLITUDE,
  OFFSET,
  DUTY
}

@Component({
  selector: 'fgm-output-panel-amplitude-offset-duty',
  templateUrl: './output-panel-amplitude-offset-duty.component.html',
  styleUrls: ['./output-panel-amplitude-offset-duty.component.scss']
})
export class OutputPanelAmplitudeOffsetDutyComponent implements OnInit {

  static readonly AMPLITUDE_LENGTH = 5;
  static readonly OFFSET_LENGTH = 4;
  static readonly DUTY_LENGTH = 3;



  @Input() amplitude: number;
  @Input() offset: number;
  @Input() duty: number;

  @Output() amplitudeChange: EventEmitter<number> = new EventEmitter();
  @Output() offsetChange: EventEmitter<number> = new EventEmitter();
  @Output() dutyChange: EventEmitter<number> = new EventEmitter();


  sliderDigitIndex: number = undefined;
  sliderValue: number = 0;
  sliderMinValue: number = 0;
  sliderType: UnitType = undefined

  unitType: typeof UnitType = UnitType;

  constructor() { }

  ngOnInit(): void {
  }

  setSliderDigit(unit: UnitType, index: number) {
    this.sliderType = unit;
    if(this.sliderType == UnitType.AMPLITUDE) {
      this.sliderDigitIndex = index;
      this.sliderValue = +this.amplitude.toString().padStart(OutputPanelAmplitudeOffsetDutyComponent.AMPLITUDE_LENGTH, '0')[this.sliderDigitIndex];
      this.sliderMinValue = 0;
    }
    else if(this.sliderType == UnitType.OFFSET) {
      this.sliderDigitIndex = index + 1;
      this.sliderValue = +this.offset.toString().padStart(OutputPanelAmplitudeOffsetDutyComponent.OFFSET_LENGTH, '0')[this.sliderDigitIndex];
      if(this.sliderDigitIndex == 1 && this.offset < 0)
        this.sliderValue = -this.sliderValue;
      this.sliderMinValue = this.sliderDigitIndex == 1 ? -9 : 0;
    }
    else if(this.sliderType == UnitType.DUTY) {
      this.sliderDigitIndex = index;
      this.sliderValue = +this.duty.toString().padStart(OutputPanelAmplitudeOffsetDutyComponent.DUTY_LENGTH, '0')[this.sliderDigitIndex];
      this.sliderMinValue = 0;
    }
  }

  onSliderValueChange() {
    if(this.sliderType == UnitType.AMPLITUDE) {
      const amplitudeString = this.amplitude.toString().padStart(OutputPanelAmplitudeOffsetDutyComponent.AMPLITUDE_LENGTH, '0');
      this.amplitude = +(amplitudeString.substring(0, this.sliderDigitIndex) + this.sliderValue + amplitudeString.substring( this.sliderDigitIndex + 1));
    }
    else if(this.sliderType == UnitType.OFFSET) {
      const offsetString = this.offset.toString().padStart(OutputPanelAmplitudeOffsetDutyComponent.OFFSET_LENGTH, '0');
      if(this.sliderValue < 0)
        this.offset = +(this.sliderValue + offsetString.substring( this.sliderDigitIndex + 1));
      else
        this.offset = +(offsetString.substring(0, this.sliderDigitIndex) + this.sliderValue + offsetString.substring( this.sliderDigitIndex + 1));
    }
    else if(this.sliderType == UnitType.DUTY) {
      const dutyString = this.duty.toString().padStart(OutputPanelAmplitudeOffsetDutyComponent.DUTY_LENGTH, '0');
      this.duty = +(dutyString.substring(0, this.sliderDigitIndex) + this.sliderValue + dutyString.substring( this.sliderDigitIndex + 1));
    }  
  }

  addToSliderDigit(addValue: number) {
    if(this.sliderType == UnitType.AMPLITUDE) {
      const newAmplitude = this.amplitude + (addValue * Math.pow(10, OutputPanelAmplitudeOffsetDutyComponent.AMPLITUDE_LENGTH - this.sliderDigitIndex - 1));
      if(newAmplitude < 0)
        this.amplitude = 0;
      else if(newAmplitude > 20000)
        this.amplitude = 20000;
      else
        this.amplitude = newAmplitude;
    }
    else if(this.sliderType == UnitType.OFFSET) {
      const newOffset = this.offset + (addValue * Math.pow(10, OutputPanelAmplitudeOffsetDutyComponent.OFFSET_LENGTH - this.sliderDigitIndex - 1));
      if(newOffset <= -1000)
        this.offset = -999;
      else if(newOffset >= 1000)
        this.offset = 999;
      else
        this.offset = newOffset;
    }
    else if(this.sliderType == UnitType.DUTY) {
      const newDuty = this.duty + (addValue * Math.pow(10, OutputPanelAmplitudeOffsetDutyComponent.DUTY_LENGTH - this.sliderDigitIndex - 1));
      if(newDuty < 0)
        this.duty = 0;
      else if(newDuty > 999)
        this.duty = 999;
      else
        this.duty = newDuty;
    }

    this.setSliderDigit(this.sliderType, this.sliderDigitIndex - (this.sliderType == UnitType.OFFSET ? 1 : 0));
    this.emitChange();
  }

  emitChange() {
    if(this.sliderType == UnitType.AMPLITUDE)
      this.amplitudeChange.emit(this.amplitude);
    else if(this.sliderType == UnitType.OFFSET)
      this.offsetChange.emit(this.offset);
    else if(this.sliderType == UnitType.DUTY)
      this.dutyChange.emit(this.duty);
  }
}
