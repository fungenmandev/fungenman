import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainOutputPanelComponent } from './main-output-panel.component';

describe('MainOutputPanelComponent', () => {
  let component: MainOutputPanelComponent;
  let fixture: ComponentFixture<MainOutputPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainOutputPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainOutputPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
