import { Component, OnInit } from '@angular/core';
import { Channel } from '../models/constants';

@Component({
  selector: 'fgm-main-output-panel',
  templateUrl: './main-output-panel.component.html',
  styleUrls: ['./main-output-panel.component.scss']
})
export class MainOutputPanelComponent implements OnInit {

  channel1: Channel = Channel.One;
  channel2: Channel = Channel.Two;

  constructor() { }

  ngOnInit(): void {
  }

}
