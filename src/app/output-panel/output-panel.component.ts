import { Component, OnInit, Input, Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { BaseChannelService } from '../services/base-channel.service';
import { Channel1Service } from '../services/channel1.service';
import { Channel2Service } from '../services/channel2.service';
import { PhraseService } from '../services/phrase.service';
import { FrequencyUnit, Channel } from '../models/constants';
import { SerialService } from '../services/serial.service';

@Component({
  selector: 'fgm-output-panel',
  templateUrl: './output-panel.component.html',
  styleUrls: ['./output-panel.component.scss']
})
export class OutputPanelComponent implements OnInit {

  private channelService: BaseChannelService;

  @Input() channel: Channel;

  wave: string = 'W05PARTIAL_SINE';
  frequency: number = 1000000;
  frequencyUnit: FrequencyUnit = FrequencyUnit.Hz;
  amplitude: number = 5000;
  offset: number = 0;
  duty: number = 500;

  manualValuesForm: FormGroup;
  frequencyValueControl: FormControl;
  frequencyUnits = FrequencyUnit;


  constructor(private injector: Injector, private phraseService: PhraseService, private serialService: SerialService, private fb: FormBuilder) {
    this.frequencyValueControl = fb.control(this.frequency / 100, [Validators.required, Validators.min(0), Validators.max(999999999.99)]);
    this.manualValuesForm = fb.group({
      frequency: this.frequencyValueControl
    })
  }

  ngOnInit(): void {
    if (this.channel == Channel.One)
      this.channelService = this.injector.get(Channel1Service);
    else if (this.channel == Channel.Two)
      this.channelService = this.injector.get(Channel2Service);
  }

  onFrequencyScaleChange() {
    if (this.frequencyUnit == FrequencyUnit.MHz) {
      this.frequencyValueControl.setValidators([Validators.required, Validators.min(0), Validators.max(99.99999999)])
      this.frequencyValueControl.setValue(this.frequency / 100000000);
    }
    else if (this.frequencyUnit == FrequencyUnit.kHz) {
      this.frequencyValueControl.setValidators([Validators.required, Validators.min(0), Validators.max(99999.99999)])
      this.frequencyValueControl.setValue(this.frequency / 100000);
    }
    else {
      this.frequencyValueControl.setValidators([Validators.required, Validators.min(0), Validators.max(99999999.99)])
      this.frequencyValueControl.setValue(this.frequency / 100);
    }
    this.sendFrequency();
  }

  setFrequency(values: {frequency: number, unit: FrequencyUnit}) {
    this.frequency = values.frequency;
    this.frequencyUnit = values.unit;

    if (this.frequencyUnit == FrequencyUnit.MHz)
      this.frequencyValueControl.setValue(this.frequency / 100000000);
    else if (this.frequencyUnit == FrequencyUnit.kHz)
      this.frequencyValueControl.setValue(this.frequency / 100000);
    else
      this.frequencyValueControl.setValue(this.frequency / 100);

    this.sendFrequency();
  }

  setAmplitude(newAmplitude: number) {
    this.amplitude = newAmplitude;
    this.sendAmplitude();
  }

  setOffset(newOffset: number) {
    this.offset = newOffset;
    this.sendOffset();
  }
  
  setDuty(newDuty: number) {
    this.duty = newDuty;
    this.sendDuty();
  }

  setWave(newWave: string) {
    this.wave = newWave;
    this.sendWave();
  }

  setManualValues() {
    if (this.frequencyUnit == FrequencyUnit.MHz) {
      this.frequencyValueControl.setValue(Math.round(this.frequencyValueControl.value * 100000000) / 100000000);
      this.frequency = this.frequencyValueControl.value * 100000000;
    }
    else if (this.frequencyUnit == FrequencyUnit.kHz) {
      this.frequencyValueControl.setValue(Math.round(this.frequencyValueControl.value * 100000) / 100000);
      this.frequency = this.frequencyValueControl.value * 100000;
    }
    else {
      this.frequencyValueControl.setValue(Math.round(this.frequencyValueControl.value * 100) / 100);
      this.frequency = this.frequencyValueControl.value * 100;
    }

    this.sendFrequency();
  }

  sendFrequency() {
    console.log(this.phraseService.getFrequencyChangePhrase(this.channel, this.frequency, this.frequencyUnit));
    this.serialService.write(this.phraseService.getFrequencyChangePhrase(this.channel, this.frequency, this.frequencyUnit));
  }

  sendAmplitude() {
    console.log(this.phraseService.getAmplitudeChangePhrase(this.channel, this.amplitude));
    this.serialService.write(this.phraseService.getAmplitudeChangePhrase(this.channel, this.amplitude));
  }

  sendOffset() {
    console.log(this.phraseService.getOffsetChangePhrase(this.channel, this.offset));
    this.serialService.write(this.phraseService.getOffsetChangePhrase(this.channel, this.offset));
  }

  sendDuty() {
    console.log(this.phraseService.getDutyChangePhrase(this.channel, this.duty));
    this.serialService.write(this.phraseService.getDutyChangePhrase(this.channel, this.duty));
  }

  sendWave() {
    console.log(this.phraseService.getWaveChangePhrase(this.channel, this.wave));
    this.serialService.write(this.phraseService.getWaveChangePhrase(this.channel, this.wave));
  }

}
