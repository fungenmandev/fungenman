import { Component } from '@angular/core';

@Component({
  selector: 'fgm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'FunGenMan';
}
