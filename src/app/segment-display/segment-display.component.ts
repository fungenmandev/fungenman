import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter, AfterViewChecked, HostListener } from '@angular/core';

@Component({
  selector: 'fgm-segment-display',
  templateUrl: './segment-display.component.html',
  styleUrls: ['./segment-display.component.scss']
})
export class SegmentDisplayComponent implements OnInit, AfterViewChecked {
  
  @Input() displayValue: string = '';
  @Input() showOff: boolean = true;
  @Input() colorOn: string = "#e95d0f";
  @Input() colorOnSelected: string = "#00ff00";
  @Input() colorOff: string = "#4b1e05";
  @Input() backgroundColor: string = "#000000";
  @Input() fontSizePx: number = 35;
  @Input() fontName: string = 'DSEG7-Classic';
  @Input() paddingLeft: number = 6;

  @Input() selectedCharacterIndex: number;
  
  @Output() selectedIndex: EventEmitter<number> = new EventEmitter<number>();
  
  @ViewChild('wrapper') wrapper: ElementRef;
  @ViewChild('displayWrapper') displayWrapper: ElementRef;

  @ViewChild('display') display: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewChecked(): void {
    const height = `${this.display.nativeElement.offsetHeight}px`;
    this.wrapper.nativeElement.style.height = height;
    this.displayWrapper.nativeElement.style.height = height;

    const width = `${this.display.nativeElement.offsetWidth + 2*this.paddingLeft}px`;
    this.displayWrapper.nativeElement.style.width = width;
    this.displayWrapper.nativeElement.style.paddingLeft = `${this.paddingLeft}px`;
  }

  getMask(): string {
    let mask = ''
    for (var i = 0; i < this.displayValue.length; i++) {
      switch(this.displayValue.charAt(i)) {
        case ',': mask += ' '; break;
        case '.': mask += '.'; break;
        default: mask += '8';
      }
    }
    return mask;
  }

  getValue() {
    let value = '';
    for (var i = 0; i < this.displayValue.length; i++) {
      switch(this.displayValue.charAt(i)) {
        case ',': value += ' '; break;
        case '.': value += '.'; break;
        default: value += this.displayValue.charAt(i);
      }
    }
    return value;
  }

  selectCharacter(character: string, index: number) {
    if(character != ' ' && character != '.' && character != '-') {
      this.selectedCharacterIndex = index;

      let selectedDigit = 0;
      for(let i = 0 ; i < this.selectedCharacterIndex ; i++) {
        if(this.displayValue.charAt(i) >= '0' && this.displayValue.charAt(i) <= '9')
          selectedDigit++;
      }
      this.selectedIndex.emit(selectedDigit);
    }
  }
}
