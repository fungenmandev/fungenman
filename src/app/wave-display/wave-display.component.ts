import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input, OnChanges, SimpleChanges } from '@angular/core';

import * as d3 from 'd3';
import { WaveDataService } from '../services/wave-data.service';

@Component({
  selector: 'fgm-wave-display',
  templateUrl: './wave-display.component.html',
  styleUrls: ['./wave-display.component.scss']
})
export class WaveDisplayComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() wave: string;
  @Input() duty: number;

  @ViewChild('wave') private waveSvg: ElementRef;

  private margin = { top: 0, right: 1, bottom: 1, left: 0 };
  private waveData: { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> };
  private innerWidth: number;
  private innerHeight: number;
  private xScale: d3.ScaleLinear<number, number>;
  private yScale: d3.ScaleLinear<number, number>;
  private svg: d3.Selection<any, unknown, null, undefined>;

  constructor(private waveDataService: WaveDataService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.getWaveData();
    this.drawAxes();
    this.drawFunction();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.waveSvg && (changes.wave || changes.duty)) {      
      d3.select(this.waveSvg.nativeElement).select("g").remove();
      this.getWaveData();
      this.drawAxes();
      this.drawFunction();
    }
  }

  private computeScales(xMin: number, xMax: number, yMin: number, yMax: number) {
    const width = 300/*this.waveSvg.nativeElement.clientWidth*/;
    const height = 300 /* this.waveSvg.nativeElement.clientHeight;*/

    this.innerWidth = width - this.margin.left - this.margin.right;
    this.innerHeight = height - this.margin.bottom - this.margin.top;

    this.xScale = d3.scaleLinear().domain([xMin, xMax]).range([0, this.innerWidth]);
    this.yScale = d3.scaleLinear().domain([yMax, yMin]).range([0, this.innerHeight]);
  }

  private getWaveData() {
    this.waveData = this.waveDataService.getWave(this.wave, this.duty);
    this.computeScales(this.waveData.xMin, this.waveData.xMax, this.waveData.yMin, this.waveData.yMax);

    this.svg = d3.select(this.waveSvg.nativeElement)
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
  }

  private drawFunction() {
    let plot = d3.line()
      .x(d => this.xScale(d[0]))
      .y(d => this.yScale(d[1]))

    this.svg.append('path')
      .datum(this.waveData.data)
      .attr('fill', 'none')
      .attr('stroke', 'yellow')
      .attr('stroke-width', 3)
      .attr('d', plot)
  }

  private drawAxes() {
    this.drawXAxisGrid(this.waveData.xMin, this.waveData.xMax);
    this.drawYAxisGrid(this.waveData.yMin, this.waveData.yMax);
  }

  private drawXAxisGrid(min: number, max: number) {
    let values = [min, min + (max - min) / 2, max];

    let xAxisGrid = d3.axisBottom(this.xScale)
      .tickSize(-this.innerHeight)
      .tickFormat(() => '')
      .tickValues(values);

    let grid = this.svg.append('g')
      .attr('transform', `translate(0,${this.innerHeight})`)
      .call(xAxisGrid);

    grid.selectAll("line,path").style("stroke", "red")
    grid.selectAll("path").style("stroke-width", 0);

    values = [min + (max - min) / 4, max - (max - min) / 4];

    xAxisGrid = d3.axisBottom(this.xScale)
      .tickSize(-this.innerHeight)
      .tickFormat(() => '')
      .tickValues(values);

    grid = this.svg.append('g')
      .attr('transform', `translate(0,${this.innerHeight})`)
      .call(xAxisGrid);

    grid.selectAll("line,path").style("stroke-dasharray", "5 5").style("stroke", "red")
    grid.selectAll("path").style("stroke-width", 0);
  }

  private drawYAxisGrid(min: number, max: number) {
    let values = [min, min + (max - min) / 2, max];
    let yAxisGrid = d3.axisRight(this.yScale)
      .tickSize(this.innerWidth)
      .tickFormat(() => '')
      .tickValues(values);

    let grid = this.svg.append('g')
      .attr('transform', `translate(0,0)`)
      .call(yAxisGrid);

    grid.selectAll("line,path").style("stroke", "red")
    grid.selectAll("path").style("stroke-width", 0);

    values = [min + (max - min) / 4, max - (max - min) / 4];

    yAxisGrid = d3.axisRight(this.yScale)
      .tickSize(this.innerWidth)
      .tickFormat(() => '')
      .tickValues(values);

    grid = this.svg.append('g')
      .attr('transform', `translate(0,0)`)
      .call(yAxisGrid);

    grid.selectAll("line,path").style("stroke-dasharray", "5 5").style("stroke", "red")
    grid.selectAll("path").style("stroke-width", 0);
  }
}
