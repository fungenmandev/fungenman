import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaveDisplayComponent } from './wave-display.component';

describe('WaveDisplayComponent', () => {
  let component: WaveDisplayComponent;
  let fixture: ComponentFixture<WaveDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaveDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaveDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
