import { Injectable, ModuleWithComponentFactories } from '@angular/core';
import { SerialPhrase } from '../models/serial-phrase';
import { SerialPhraseConstants, FrequencyUnit, Channel, WaveFormCodes } from '../models/constants';

@Injectable({
  providedIn: 'root'
})
export class PhraseService {

  constructor() { }

  getModelPhrase(): SerialPhrase {
    return { key: SerialPhraseConstants.MODEL }
  }

  getSerialNumberPhrase(): SerialPhrase {
    return { key: SerialPhraseConstants.SERIAL_NUMBER }
  }

  getFrequencyChangePhrase(channel: Channel, value: number, unit: FrequencyUnit): SerialPhrase {
    const key = (channel == Channel.One ? SerialPhraseConstants.C1_FREQUENCY : SerialPhraseConstants.C2_FREQUENCY);
    const message = value.toString().replace('.', '') + ',' + unit;

    if (key && message)
      return { key, message };

    return undefined;
  }

  getAmplitudeChangePhrase(channel: Channel, value: number): SerialPhrase {
    const key = (channel == Channel.One ? SerialPhraseConstants.C1_AMPLITUDE : SerialPhraseConstants.C2_AMPLITUDE);
    const message = value.toString();

    if (key && message)
      return { key, message };

    return undefined;
  }

  getDutyChangePhrase(channel: Channel, value: number): SerialPhrase {
    const key = (channel == Channel.One ? SerialPhraseConstants.C1_DUTY : SerialPhraseConstants.C2_DUTY);
    const message = value.toString();

    if (key && message)
      return { key, message };

    return undefined;
  }

  getOffsetChangePhrase(channel: Channel, value: number): SerialPhrase {
    const key = (channel == Channel.One ? SerialPhraseConstants.C1_OFFSET : SerialPhraseConstants.C2_OFFSET);
    const message = (value + 1000).toString();

    if (key && message)
      return { key, message };

    return undefined;
  }

  getWaveChangePhrase(channel: Channel, waveType: string): SerialPhrase {
    const key = (channel == Channel.One ? SerialPhraseConstants.C1_WAVE : SerialPhraseConstants.C2_WAVE);
    const message = WaveFormCodes[waveType];

    if (key && message)
      return { key, message };

    return undefined;
  }
}
