import { Injectable } from '@angular/core';
import * as SerialPort from 'serialport';
import { from, Observable, Subject } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';
import { SerialPhrase } from '../models/serial-phrase';
import { ProtocolService } from './protocol.service';

@Injectable({
  providedIn: 'root'
})
export class SerialService {

  private _connected: boolean = false;
  private _answerSubject: Subject<SerialPhrase> = new Subject<SerialPhrase>();
  private serialPort: typeof SerialPort;
  private connection: SerialPort;

  constructor(private protocolService: ProtocolService) { 
    this.serialPort = window.require('serialport');
  }

  public get answerSubject(): Subject<SerialPhrase> {
    return this._answerSubject;
  }

  public connect(path: string) {
    this.connection = new this.serialPort(path, { baudRate: 115200 });

    this.connection.on('open', () => {
      this._connected = true;
      this.answerSubject.next(this.protocolService.getConnectionStatusAnswer(this._connected));
    })

    this.connection.on('close', () => {
      this._connected = false;
      this.answerSubject.next(this.protocolService.getConnectionStatusAnswer(this._connected));
    })

    this.connection.on('data', data => {
      this.processData(data.toString());
    })
  }

  public disconnect() {
    this.connection.close();
  }

  
  public getAvailablePorts(): Observable<SerialPort.PortInfo[]> {
    return from(this.serialPort.list());
  }

  public write(message: SerialPhrase): void {
    const serialMessage = this.protocolService.processMessage(message);
    console.log(serialMessage);
    if(this.connection && serialMessage)
      this.connection.write(serialMessage); 
  }

  private processData(data: string) {
    this.protocolService.processAnswers(data).forEach(answer => this._answerSubject.next(answer));
  }
}
