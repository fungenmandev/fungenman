import { TestBed } from '@angular/core/testing';

import { Channel1Service } from './channel1.service';

describe('Channel2Service', () => {
  let service: Channel1Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Channel1Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
