import { TestBed } from '@angular/core/testing';

import { WaveDataService } from './wave-data.service';

describe('WaveDataService', () => {
  let service: WaveDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WaveDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
