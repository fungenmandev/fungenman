import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { POINT_CONVERSION_COMPRESSED } from 'constants';

@Injectable({
  providedIn: 'root'
})
export class WaveDataService {

  public static readonly GRAPH_X_MIN = 0;
  public static readonly GRAPH_X_MAX = 2047;

  constructor() { }

  getWave(wave: string, duty: number): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    switch (wave) {
      case 'W01SINE': return this.getPartialSine(0.50);
      case 'W02SQUARE': return this.getPulse(0.5);
      case 'W03PULSE': return this.getPulse(duty);
      case 'W04TRIANGLE': return this.getTriangle(duty);
      case 'W05PARTIAL_SINE': return this.getPartialSine(duty);
      case 'W06CMOS':
        const waveDataCMOS = this.getPulse(0.75);
        waveDataCMOS.data.forEach(data => data[1] = data[1] < 0 ? 0 : data[1]);
        return waveDataCMOS;
      case 'W07DC': return this.getDC();
      case 'W08HALF_WAVE':
        const waveDataHalfWave = this.getPartialSine(0.50);
        waveDataHalfWave.data.forEach(data => data[1] = data[1] < 0 ? 0 : data[1]);
        return waveDataHalfWave;
      case 'W09FULL_WAVE':
        const waveDataFullWave = this.getPartialSine(0.50);
        waveDataFullWave.data.forEach(data => data[1] = data[1] < 0 ? -data[1] : data[1]);
        return waveDataFullWave;
      case 'W10POS_LADDER': return this.getLadder();
      case 'W11NEG_LADDER': 
        const waveLadder = this.getLadder();
        waveLadder.data.forEach(data => data[1] = -data[1]);
        return waveLadder;
      case 'W12NOISE': return this.getNoise();
      case 'W13EXP_RISE': 
        const waveRise = this.getExpDecay();
        waveRise.data.forEach(data => data[1] = -data[1]);
        const yMinTemp = waveRise.yMin;
        waveRise.yMin = -waveRise.yMax;
        waveRise.yMax = -yMinTemp;
        return waveRise;
      case 'W14EXP_DECAY': return this.getExpDecay();
      case 'W15MULTI_TONE': return this.getMultiTone();
      case 'W16SINC': return this.getSinc();
      case 'W17LORENZ': return this.getLorenz();
      case 'W18ARBITRARY': return this.getPartialSine(0.50);
    }
  }

  getPulse(duty: number): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    const min: number = 0;
    const max: number = 1;

    let xScale = d3.scaleLinear().domain([min, max]).range([WaveDataService.GRAPH_X_MIN, WaveDataService.GRAPH_X_MAX]);

    const data = [];
    data.push([xScale(0), 0.9]);
    data.push([xScale(duty * 0.5), 0.9]);
    data.push([xScale(duty * 0.5), -0.9]);
    data.push([xScale(0.5),-0.9]);
    data.push([xScale(0.5), 0.9]);
    data.push([xScale(0.5 + duty * 0.5), 0.9]);
    data.push([xScale(0.5 + duty * 0.5), -0.9]);
    data.push([xScale(1), -0.9]);

    const yMin = -1;
    const yMax = 1

    return { xMin: xScale(min), xMax: xScale(max), yMin, yMax, data };
  }

  getTriangle(duty: number): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    const min: number = 0;
    const max: number = 1;

    let xScale = d3.scaleLinear().domain([min, max]).range([WaveDataService.GRAPH_X_MIN, WaveDataService.GRAPH_X_MAX]);

    const data = [];
    data.push([xScale(0), -1]);
    data.push([xScale(duty * 0.5), 1]);
    data.push([xScale(0.5), -1]);
    data.push([xScale(0.5 + duty * 0.5), 1]);
    data.push([xScale(1), -1]);

    const yMin = d3.min(data, d => d[1]);
    const yMax = d3.max(data, d => d[1])

    return { xMin: xScale(min), xMax: xScale(max), yMin, yMax, data };
  }

  getPartialSine(duty: number): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    let min: number = 0;
    const max: number = 2 * Math.PI;

    let xScale = d3.scaleLinear().domain([min, max / 4]).range([WaveDataService.GRAPH_X_MIN, duty * WaveDataService.GRAPH_X_MAX / 2]);

    const step = (max - min) / 2048;
    const data = [];
    for (let i = min; i < max / 4; i = i + step)
      data.push([xScale(i), Math.sin(i)]);
    min = xScale(min);

    xScale = d3.scaleLinear().domain([max / 4, max / 2]).range([duty * WaveDataService.GRAPH_X_MAX / 2, WaveDataService.GRAPH_X_MAX / 2]);
    for (let i = max / 4; i < max / 2; i = i + step)
      data.push([xScale(i), Math.sin(i)]);

    xScale = d3.scaleLinear().domain([max / 2, 3 * max / 4]).range([WaveDataService.GRAPH_X_MAX / 2, WaveDataService.GRAPH_X_MAX - duty * WaveDataService.GRAPH_X_MAX / 2]);
    for (let i = max / 2; i < 3 * max / 4; i = i + step)
      data.push([xScale(i), Math.sin(i)]);

    xScale = d3.scaleLinear().domain([3 * max / 4, max]).range([WaveDataService.GRAPH_X_MAX - duty * WaveDataService.GRAPH_X_MAX / 2, WaveDataService.GRAPH_X_MAX]);
    for (let i = 3 * max / 4; i < max; i = i + step)
      data.push([xScale(i), Math.sin(i)]);

    const yMin = d3.min(data, d => d[1]);
    const yMax = d3.max(data, d => d[1]);


    return { xMin: min, xMax: xScale(max), yMin, yMax, data };
  }

  getDC(): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    const min: number = 0;
    const max: number = 1;

    let xScale = d3.scaleLinear().domain([min, max]).range([WaveDataService.GRAPH_X_MIN, WaveDataService.GRAPH_X_MAX]);

    const data = [];
    data.push([xScale(0), 0.9]);
    data.push([xScale(1), 0.9]);

    const yMin = -1;
    const yMax = 1;

    return { xMin: xScale(min), xMax: xScale(max), yMin, yMax, data };
  }

  getLadder(): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    const min: number = 0;
    const max: number = 1;
    const yStep = 2/7;
    const xStep = 1/8;

    let xScale = d3.scaleLinear().domain([min, max]).range([WaveDataService.GRAPH_X_MIN, WaveDataService.GRAPH_X_MAX]);
    
    const data = [];
    for(let i = 0; i < 7; i++) {
      data.push([xScale(i*xStep), -1 + i*yStep]);
      data.push([xScale((i+1)*xStep), -1 + i*yStep]);
      data.push([xScale((i+1)*xStep), -1 + (i+1)*yStep]);
    }
    data.push([xScale(1), 1]);

    const yMin = -1
    const yMax = 1;

    return { xMin: xScale(min), xMax: xScale(max), yMin, yMax, data };
  }

  getNoise(): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    const min: number = 0;
    const max: number = 1;
    const xStep = (max-min) / (WaveDataService.GRAPH_X_MAX - WaveDataService.GRAPH_X_MIN);

    let xScale = d3.scaleLinear().domain([min, max]).range([WaveDataService.GRAPH_X_MIN, WaveDataService.GRAPH_X_MAX]);
    
    const data = [];
    for(let i = min; i <= max; i += xStep*10) {
      data.push([xScale(i), 2*Math.random() - 1]);
    }

    const yMin = -1;
    const yMax = 1;

    return { xMin: xScale(min), xMax: xScale(max), yMin, yMax, data };
  }

  getExpDecay(): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    const min: number = -2;
    const max: number = 4.5;
    const xStep = (max-min) / (WaveDataService.GRAPH_X_MAX - WaveDataService.GRAPH_X_MIN);

    let xScale = d3.scaleLinear().domain([min, max]).range([WaveDataService.GRAPH_X_MIN, WaveDataService.GRAPH_X_MAX]);
    
    const data = [];
    for(let i = min; i <= max; i += xStep) {
      data.push([xScale(i), Math.exp(-i)-3]);
    }

    const yMin = d3.min(data, d => d[1]);
    const yMax = d3.max(data, d => d[1]);

    return { xMin: xScale(min), xMax: xScale(max), yMin, yMax, data };
  }

  getMultiTone(): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    const min: number = 0;
    const max: number = 2*Math.PI;
    const xStep = (max-min) / (WaveDataService.GRAPH_X_MAX - WaveDataService.GRAPH_X_MIN);

    let xScale = d3.scaleLinear().domain([min, max]).range([WaveDataService.GRAPH_X_MIN, WaveDataService.GRAPH_X_MAX]);
    
    const data = [];
    for(let i = min; i <= max; i += xStep) {
      data.push([xScale(i), Math.sin(i) + Math.sin(2*i) + Math.sin(3*i) + Math.sin(4*i)+ Math.sin(5*i)]);
    }

    const yMin = d3.min(data, d => d[1]);
    const yMax = d3.max(data, d => d[1]);

    return { xMin: xScale(min), xMax: xScale(max), yMin, yMax, data };
  }

  getSinc(): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    const min: number = -5*Math.PI;
    const max: number = 5*Math.PI;
    const xStep = (max-min) / (WaveDataService.GRAPH_X_MAX - WaveDataService.GRAPH_X_MIN);

    let xScale = d3.scaleLinear().domain([min, max]).range([WaveDataService.GRAPH_X_MIN, WaveDataService.GRAPH_X_MAX]);
    
    const data = [];
    for(let i = min; i <= max; i += xStep) {
      data.push([xScale(i), Math.sin(i)/i]);
    }

    const yMin = d3.min(data, d => d[1]);
    const yMax = d3.max(data, d => d[1]);

    return { xMin: xScale(min), xMax: xScale(max), yMin, yMax, data };
  }

  getLorenz(): { xMin: number, xMax: number, yMin: number, yMax: number, data: Array<[number, number]> } {
    let min: number = -7;
    const max: number = 7;

    let xScale = d3.scaleLinear().domain([min, max]).range([WaveDataService.GRAPH_X_MIN, WaveDataService.GRAPH_X_MAX]);

    const step = (max - min) / 2048;
    const data = [];
    for (let i = min; i < -0.75; i = i + step)
      data.push([xScale(i), Math.exp(i/2)]);
   
    for (let i = -0.75; i < 0.75; i = i + step)
      data.push([xScale(i), Math.cos(i/1.5)-0.19]);

    for (let i = 0.75; i < max; i = i + step)
      data.push([xScale(i), Math.exp(-i/2)]);

    const yMin = d3.min(data, d => d[1]);
    const yMax = d3.max(data, d => d[1]);


    return { xMin: min, xMax: xScale(max), yMin, yMax, data };
  }
}
