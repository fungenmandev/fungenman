import { Injectable } from '@angular/core';
import { BaseChannelService } from './base-channel.service';

@Injectable({
  providedIn: 'root'
})
export class Channel2Service extends BaseChannelService {

  constructor() { 
    super(2);
  }
}
