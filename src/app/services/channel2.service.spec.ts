import { TestBed } from '@angular/core/testing';

import { Channel2Service } from './channel2.service';

describe('Channel2Service', () => {
  let service: Channel2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Channel2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
