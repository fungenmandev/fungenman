import { Injectable } from '@angular/core';
import { SerialPhrase } from '../models/serial-phrase';
import { SerialConstants, SerialPhraseConstants, FrequencyUnit, Channel } from '../models/constants';

@Injectable({
  providedIn: 'root'
})
export class ProtocolService {

  constructor() { }

  getConnectionStatusAnswer(connected: boolean) {
    return { key: SerialPhraseConstants.CONNECTED, message: connected ? 'true' : 'false'}
  }

  processAnswers(serialString: string): SerialPhrase[] {
    const answers: SerialPhrase[] = [];

    serialString.split('.\r\n').forEach(answer => {
      if(answer != '') {
        let key: string;
        switch(answer.substr(0,5)) {
          case SerialConstants.MODEL: key = SerialPhraseConstants.MODEL; break;
          case SerialConstants.SERIAL_NUMBER: key = SerialPhraseConstants.SERIAL_NUMBER; break;
          default: key = SerialPhraseConstants.UNKNOWN; break;
        }
    
        const message = answer.substr(5);
        answers.push({ key, message});
      }
    })

    return answers;
  }

  processMessage(serialPhrase: SerialPhrase): string {
    let message: string;
    switch(serialPhrase.key) {
      case SerialPhraseConstants.MODEL: message=SerialConstants.MODEL; break;
      case SerialPhraseConstants.SERIAL_NUMBER: message=SerialConstants.SERIAL_NUMBER; break;
      case SerialPhraseConstants.C1_WAVE: message=SerialConstants.C1_WAVE; break;
      case SerialPhraseConstants.C2_WAVE: message=SerialConstants.C2_WAVE; break;
      case SerialPhraseConstants.C1_FREQUENCY: message=SerialConstants.C1_FREQUENCY; break;
      case SerialPhraseConstants.C2_FREQUENCY: message=SerialConstants.C2_FREQUENCY; break;
      case SerialPhraseConstants.C1_AMPLITUDE: message=SerialConstants.C1_AMPLITUDE; break;
      case SerialPhraseConstants.C2_AMPLITUDE: message=SerialConstants.C2_AMPLITUDE; break;
      case SerialPhraseConstants.C1_OFFSET: message=SerialConstants.C1_OFFSET; break;
      case SerialPhraseConstants.C2_OFFSET: message=SerialConstants.C2_OFFSET; break;
      case SerialPhraseConstants.C1_DUTY: message=SerialConstants.C1_DUTY; break;
      case SerialPhraseConstants.C2_DUTY: message=SerialConstants.C2_DUTY; break;
      default: return undefined;
    }   

    return message + (serialPhrase.message ? serialPhrase.message : '0') + '.\r\n';
  }
}
