import { TestBed } from '@angular/core/testing';

import { BaseChannelService } from './base-channel.service';

describe('BaseChannelService', () => {
  let service: BaseChannelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BaseChannelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
