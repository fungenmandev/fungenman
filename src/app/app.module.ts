import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbNavModule, NgbButtonsModule } from '@ng-bootstrap/ng-bootstrap';
import { NouisliderModule } from 'ng2-nouislider';

// Icons
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faSync, faPowerOff, faCheck } from '@fortawesome/free-solid-svg-icons';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MainOutputPanelComponent } from './main-output-panel/main-output-panel.component';
import { OutputPanelComponent } from './output-panel/output-panel.component';
import { SegmentDisplayComponent } from './segment-display/segment-display.component';
import { SplitPipe } from './pipes/split.pipe';
import { OutputPanelFrequencyComponent } from './output-panel-frequency/output-panel-frequency.component';
import { OutputPanelAmplitudeOffsetDutyComponent } from './output-panel-amplitude-offset-duty/output-panel-amplitude-offset-duty.component';
import { OutputPanelWaveComponent } from './output-panel-wave/output-panel-wave.component';
import { WaveDisplayComponent } from './wave-display/wave-display.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    MainOutputPanelComponent,
    OutputPanelComponent,
    SegmentDisplayComponent,
    SplitPipe,
    OutputPanelFrequencyComponent,
    OutputPanelAmplitudeOffsetDutyComponent,
    OutputPanelWaveComponent,
    WaveDisplayComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    NgbNavModule,
    NgbButtonsModule,
    NouisliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(iconLibrary: FaIconLibrary) {
    iconLibrary.addIcons(faSync, faPowerOff, faCheck);
  }
}
