import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FrequencyUnit } from '../models/constants';

@Component({
  selector: 'fgm-output-panel-frequency',
  templateUrl: './output-panel-frequency.component.html',
  styleUrls: ['./output-panel-frequency.component.scss']
})
export class OutputPanelFrequencyComponent implements OnInit {

  static readonly FREQUENCY_LENGTH = 10;

  @Input() frequency: number;
  @Input() frequencyUnit: FrequencyUnit;

  @Output() frequencyChange: EventEmitter<{frequency: number, unit: FrequencyUnit}> = new EventEmitter();

  sliderDigitIndex: number = undefined;
  sliderValue: number = 0;
  sliderMaxValue: number = 9;

  frequencyUnits = FrequencyUnit;
  
  constructor() { }

  ngOnInit(): void {
  }

  getNumberFilterFormat() {
    if (this.frequencyUnit == FrequencyUnit.kHz)
      return '5.5-5';
    else if (this.frequencyUnit == FrequencyUnit.MHz)
      return '2.8-8'
    else
      return '8.2-2';
  }

  getRealFrequency() {
    if (this.frequencyUnit == FrequencyUnit.kHz)
      return this.frequency / 100000;
    else if (this.frequencyUnit == FrequencyUnit.MHz)
    return this.frequency / 100000000;
    else
      return this.frequency / 100;
  }

  setSliderDigit(index: number) {
    this.sliderDigitIndex = index;
    this.sliderMaxValue = this.sliderDigitIndex == 0 ? 6 : 9;
    this.sliderValue = +this.frequency.toString().padStart(OutputPanelFrequencyComponent.FREQUENCY_LENGTH, '0')[this.sliderDigitIndex];
  }

  addToSliderDigit(addValue: number) {
    const newFrequency = this.frequency + (addValue * Math.pow(10, OutputPanelFrequencyComponent.FREQUENCY_LENGTH - this.sliderDigitIndex - 1));
    if(newFrequency < 0)
      this.frequency = 0;
    else if(newFrequency > 6000000000)
      this.frequency = 6000000000;
    else
      this.frequency = newFrequency;

    this.setSliderDigit(this.sliderDigitIndex);
    this.emitFrequencyChange();
  }

  onSliderValueChange() {
    const frequencyString = this.frequency.toString().padStart(OutputPanelFrequencyComponent.FREQUENCY_LENGTH, '0');
    this.frequency = +(frequencyString.substring(0, this.sliderDigitIndex) + this.sliderValue + frequencyString.substring( this.sliderDigitIndex + 1));
    if(this.frequency > 6000000000) {
      this.frequency = 6000000000;
      this.setSliderDigit(this.sliderDigitIndex);
    }
      
  }

  emitFrequencyChange() {
    this.frequencyChange.emit({frequency: this.frequency, unit: this.frequencyUnit});
  }
}
