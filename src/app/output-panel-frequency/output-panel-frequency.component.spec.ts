import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputPanelFrequencyComponent } from './output-panel-frequency.component';

describe('OutputPanelFrequencyComponent', () => {
  let component: OutputPanelFrequencyComponent;
  let fixture: ComponentFixture<OutputPanelFrequencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputPanelFrequencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputPanelFrequencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
