import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { first } from 'rxjs/operators';

import { PortInfo } from '../models/port-info';
import { SerialService } from '../services/serial.service';
import { PhraseService } from '../services/phrase.service';
import { SerialPhrase } from '../models/serial-phrase';
import { SerialPhraseConstants } from '../models/constants';

@Component({
  selector: 'fgm-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  ports: PortInfo[] = [];
  path: string;
  connected: boolean = false;
  model: string;
  serialNumber: string;

  constructor(private cd: ChangeDetectorRef, private serialService: SerialService, private phraseService: PhraseService) { }

  ngOnInit(): void {
    this.serialService.answerSubject.subscribe(answer => {
      this.processAnswer(answer);
    })
    this.refreshPorts();
  }

  refreshPorts(): void {
    this.serialService.getAvailablePorts().subscribe(
      availablePorts => {
        this.ports = [];
        availablePorts.forEach(port => {
          this.ports.push({ path: port.path, manufacturer: port.manufacturer });
        })
        if (this.ports.length > 0)
          this.path = this.ports[0].path;
      }
    )
  }

  processConnection() {
    if (this.connected)
      this.serialService.disconnect();
    else
      this.serialService.connect(this.path);
  }

  processAnswer(answer: SerialPhrase) {
    switch (answer.key) {      
      case SerialPhraseConstants.MODEL: this.model = answer.message; this.cd.detectChanges(); break;
      case SerialPhraseConstants.SERIAL_NUMBER: this.serialNumber = answer.message; this.cd.detectChanges(); break;
      case SerialPhraseConstants.CONNECTED:
        this.connected = (answer.message == 'true');
        if (this.connected) {
          this.serialService.write(this.phraseService.getModelPhrase());
          this.serialService.write(this.phraseService.getSerialNumberPhrase());
        }
        else {
          this.serialNumber = undefined;
          this.model = undefined;
          this.cd.detectChanges();
        }
        break;
    }
  }

}
