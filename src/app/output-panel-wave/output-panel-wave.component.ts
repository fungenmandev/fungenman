import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WaveForm } from '../models/constants';

@Component({
  selector: 'fgm-output-panel-wave',
  templateUrl: './output-panel-wave.component.html',
  styleUrls: ['./output-panel-wave.component.scss']
})
export class OutputPanelWaveComponent implements OnInit {

  WaveForm = WaveForm;
  @Input() wave: string;

  @Output() waveChange: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  changeWaveForm() {
    this.emitWaveChange();
  }

  emitWaveChange() {
    this.waveChange.emit(this.wave);
  }

}
