import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputPanelWaveComponent } from './output-panel-wave.component';

describe('OutputPanelWaveComponent', () => {
  let component: OutputPanelWaveComponent;
  let fixture: ComponentFixture<OutputPanelWaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputPanelWaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputPanelWaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
