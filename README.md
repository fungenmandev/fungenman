# FunGenMan

FunGenMan is a software designed to use the JDS6600 DDS Signal Generator/Counter from your computer. The idea is to use it from Windows, Linux or MacOS Platform.

The project is currently under construction, but it should come in time...

Technically, this is an Angular project with Electron on top of it.

## IMPORTANT

Because of version compatibility with Electron and the serialport npm module, you must copy the file `ref/bindings.node` in `node_modules\@serialport\bindings\build\Release` after the `npm install`. Hope to find a better solution soon ! 

## Try it

To try, read first the important note, then just clone the project, `npm install`, copy the `ref/bindings.node` file and `npm start`.

To build it on Windows, read first the important note, just clone the project, `npm install`, copy the `ref/bindings.node` file and `npm run release`. You should find the portable executable in the `dist/` directory.